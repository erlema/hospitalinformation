package mappe.del1.hospital;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Erlend Matre
 * Department class
 * This class is for departments of hospitals, based on the class diagram
 */

public class Department {
    /**
     * In the class diagram it was not any List for patients and employee, but i felt it was much simpler if
     * there was two own list for those two. Also it made it easier to add and remove people from the list,
     * and also remove people is easier. Otherwise I tried following the class diagram.
     */
    private String departmentName; //name of the department
    private List<Employee> employeeList = new ArrayList<Employee>(); //list of employees, made it an ArrayList, unsure if allowed
    private List<Patient> patientList = new ArrayList<Patient>(); // list of Patients, -||-

    /**
     * constructor, creates department with given name
     * @param departmentName name of department
     */
    public Department (String departmentName){
        this.departmentName = departmentName;
    }

    /**
     * method for getting department name
     * @return name of department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * sets the name of the department
     * @param departmentName new name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * get a list of all employees
     * @return list of employees
     */
    public List<Employee> getEmployees(){
        List<Employee> copyOfEmployeeList = new ArrayList<Employee>();
        for(Employee e: employeeList){ //There are multiple ways of solving this, some may be easier
            copyOfEmployeeList.add(e);// but I went with this
        }
        return  employeeList;
    }

    /**
     * method for adding a new employee
     * @param newEmployee the new employee
     */
    public void addEmployees(Employee newEmployee){
        if (!employeeList.contains(newEmployee)) {
            employeeList.add(newEmployee);
            System.out.println("Success"); //  Does not need to print success or failure, but it was the easiest way
        }else System.out.println("Failure"); //of figuring out if they were added or not, can be removed
    }                                        //this goes for all "add-methods"
    /**
     * get a list of all patients
     * @return list of patients
     */
    public List<Patient> getPatients(){
        List<Patient> copyOfPatientList = new ArrayList<Patient>();
        for(Patient p: patientList){
            copyOfPatientList.add(p);
        }
        return copyOfPatientList;
    }

    /**
     * method for new patient
     * @param newPatient new patient
     */
    public void addPatient(Patient newPatient){ //check addEmployee
        if (!patientList.contains(newPatient)) {
            patientList.add(newPatient);
            System.out.println("Success");//only for clarity, can just be removed
        } else System.out.println("Failure"); //only for clarity, can just be removed
    }

    /**
     * method for removing person, check list of employee and patients
     * @param person person to be removed
     * @throws RemoveException throws exception if no person is found
     */
    public void remove(Person person) throws RemoveException{ //check addEmployee
        if (employeeList.contains(person)) {
            employeeList.remove(person);
            System.out.println("Employee removed"); //only for clarity, can just be removed
        }else if (patientList.contains(person)){
            patientList.remove(person);
            System.out.println("Patient removed"); //only for clarity
        }
        else throw new RemoveException("Person was not found");
    }

    /**
     * toString, should be fixed to print better looking information
     * @return String
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                '}';
    }
}
