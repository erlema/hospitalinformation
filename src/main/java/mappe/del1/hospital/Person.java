package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Person class, abstract
 */


public abstract class Person {
    private String firstName; //first name
    private String lastName; // last name
    private String socialSecurityNumber; //SSN

    /**
     * Constructor, does not check if the names are empty or if SSN is numbers
     * could do that, but I feel it is better to do where it is being sent in.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * gets first name
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * sets first name
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * sets firstName
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * sets last name
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * gets SSN
     * @return SSN
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * sets SNN
     * dont know if this is possible, but whatever
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * gets full name
     * @return full name
     */

    public String getFullName(){
        return getFirstName() + " " + getLastName();
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
