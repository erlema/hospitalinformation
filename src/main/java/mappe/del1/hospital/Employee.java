package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Employee class, extends Person
 * extends to Doctor and Nurse
 */


public class Employee extends Person{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method
     * @return
     */

    @Override
    public String toString() {
        return super.toString();
    }
}
