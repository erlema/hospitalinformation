package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Patient class, extends Person, implements Diagnosable
 * based on class diagram
 */


public class Patient extends Person implements Diagnosable{
    private String diagnosis = ""; // diagnosis, base is no text

    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * gets diagnosis
     * @return
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * sets diagnosis
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * toString
     * @return
     */
    @Override
    public String toString() {
        return "Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                "firstName='" + getFirstName() + '\'' +
                "lastName='" + getLastName() + '\'' +
                '}';
    }
}
