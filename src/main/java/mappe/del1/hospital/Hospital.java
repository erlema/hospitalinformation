package mappe.del1.hospital;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Erlend Matre
 * Hospital Class
 * Based on Class diagram and tasks
 */


public class Hospital {
    /**
     * Made a list for departments in the hospital, for the same reasons in the Department Class.
     * Other than that the class diagram is followed
     */
    private String hospitalName; //name of hospital
    private List<Department> departmentList= new ArrayList<Department>(); //department list

    /**
     * Constructor with given hospital name
     * @param hospitalName
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
    }

    /**
     *
     * @return hospital name
     */

    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Method for getting all departments
     * @return list of departments
     */
    public List<Department> getDepartments(){
        List<Department> copyOfDepartmentList = new ArrayList<Department>(); //ArrayList allowed?
        for(Department d: departmentList){ //Exist more ways of doing this
            copyOfDepartmentList.add(d);
        }
        return copyOfDepartmentList;
    }

    /**
     * method of adding new department
     * @param newDepartment the new department
     */
    public void addDepartment (Department newDepartment){
        if (!departmentList.contains(newDepartment)){
            departmentList.add(newDepartment);
            System.out.println("Success");
        }else System.out.println("Failure"); //does not need to print out anything, but it made it visible
        // could make it a boolean value, or Exception
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                '}';
    }
}
