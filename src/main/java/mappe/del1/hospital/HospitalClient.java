package mappe.del1.hospital;
/**
 * @author Erlend Matre
 *
 */


public class HospitalClient {

    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olavs Hosptial");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println(hospital.getDepartments().size());
        Department emergency = hospital.getDepartments().get(0);
        Employee employee = emergency.getEmployees().get(0);
        try{
            emergency.remove(employee);
        }catch (RemoveException e){
            e.printStackTrace();
        }
        Patient nonExistingPatient = new Patient("Charles" , "Baker", "08038573654");
        try{
            emergency.remove(nonExistingPatient);
        }catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
