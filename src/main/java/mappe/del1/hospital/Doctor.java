package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Abstract Doctor class, extends Employee
 * extends to GeneralPractioner and Surgeon
 */


public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * sets the diagnose of given patient (only GeneralPractitioner and Surgeon can do this)
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
