package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * RemoveException class, extends Exception
 */


public class RemoveException extends Exception {

    private final static long serialVersionUID = 1L; // Do not really know ??

    public RemoveException(String errorMessage){
        super(errorMessage); // creates error message
    }
}
