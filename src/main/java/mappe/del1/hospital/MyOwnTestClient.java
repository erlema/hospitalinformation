package mappe.del1.hospital;


/**
 * @author Erlend Matre
 *This class was for me to check on different methods and classes, not apart of the task, but feel free to check it out.
 *Found out that all works as intended, however there are maybe some methods that I feel like are missing
 *The project and all its classes is made based on the class diagram and instructions.
 */


public class MyOwnTestClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olavs Hospital");
        Department emergency = new Department("Emergency");
        System.out.println(emergency.getDepartmentName());
        hospital.addDepartment(emergency);
        Surgeon employee1 = new Surgeon("Karl","Mark", "");
        GeneralPractitioner employee2 = new GeneralPractitioner("Jens", "Stoltenberg", "");
        emergency.addEmployees(employee1);
        emergency.addEmployees(employee2);
        emergency.addEmployees(new Nurse("Mark", "Zuckerberg", ""));
        emergency.addEmployees(employee1);
        System.out.println(emergency.getEmployees());
        Patient patient1 = new Patient("Hall", "Kar", "");
        Patient patient2 = new Patient("Maren", "Kar", "");
        emergency.addPatient(patient1);
        emergency.addPatient(patient2);
        emergency.addPatient(new Patient("Nøkkel", "Fyr", ""));
        emergency.addPatient(patient1);
        System.out.println(emergency.getPatients());
        System.out.println(patient1.getDiagnosis());
        employee1.setDiagnosis(patient1, "He is sick");
        System.out.println(patient1.getDiagnosis());
        emergency.setDepartmentName("ER");
        System.out.println(emergency.getDepartmentName());
        hospital.addDepartment(new Department("Surgery Place"));
        System.out.println(hospital.getHospitalName());
        System.out.println(hospital.getDepartments());

    }
}
