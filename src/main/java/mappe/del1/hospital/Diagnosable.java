package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Interface
 */


public interface Diagnosable {
    public abstract void setDiagnosis(String diagnosis);
}
