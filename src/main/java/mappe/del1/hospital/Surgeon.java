package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Surgeon class, extends Doctor
 */


public class Surgeon extends Doctor{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * sets diagnose of given patient
     * @param patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
