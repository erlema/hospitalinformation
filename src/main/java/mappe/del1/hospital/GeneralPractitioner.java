package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * GeneralPractioner class, extends Doctor
 */


public class GeneralPractitioner extends Doctor{
    /**
     * constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * sets diagnose of patient
     * @param patient
     * @param diagnosis
     */

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
