package mappe.del1.hospital;
/**
 * @author Erlend Matre
 * Nurse class, extends Employee
 */


public class Nurse extends Employee{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString
     * @return
     */

    @Override
    public String toString() {
        return super.toString();
    }
}
