package mappe.del1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SurgeonTest {
    /**
     * Made some simple test to check some of the code
     */

    @Test
    void setDiagnosis() {
        Patient patient = new Patient("Charles","Baker", "04045755831");
        assertTrue(patient.getDiagnosis()=="");
        Surgeon surgeon = new Surgeon("Man", "Carl", "4839965040");
        surgeon.setDiagnosis(patient, "He is sick");
        assertTrue(patient.getDiagnosis()=="He is sick");
    }
}