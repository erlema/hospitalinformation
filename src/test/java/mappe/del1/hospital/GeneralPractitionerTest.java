package mappe.del1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GeneralPractitionerTest {
    /**
     * Made some simple test to check some of the code
     */

    @Test
    void setDiagnosis() {
        Patient patient = new Patient("Charles","Baker", "04045755831");
        assertTrue(patient.getDiagnosis()=="");
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("Man", "Carl", "4839965040");
        generalPractitioner.setDiagnosis(patient, "He is sick");
        assertTrue(patient.getDiagnosis()=="He is sick");
    }
}