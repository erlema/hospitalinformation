package mappe.del1.hospital;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {
    /**
     * Made some simple test to check some of the code
     * Also the two test of the remove method
     */

    @Test
    void getDepartmentName() {
        Department department = new Department("Emergency");
        assertTrue("Emergency" == department.getDepartmentName());
    }

    @Test
    void setDepartmentName() {
        Department department = new Department("Emergency");
        department.setDepartmentName("Hospital");
        assertFalse(department.getDepartmentName()=="Emergency");
    }


    @Test
    void removePositive() {
        Department department = new Department("Emergency");
        Employee randomEmployee = new Employee("Charles", "Baker", "01018757219");
        department.addEmployees(randomEmployee);
        boolean tester = true;
        try{
            department.remove(randomEmployee);
        } catch (RemoveException e){
            tester = false;
        }
        assertTrue(tester == true);
    }
    @Test
    void removeNegative() {
        Department department = new Department("Emergency");
        Employee randomEmployee = new Employee("Charles", "Baker", "01018757219");
        boolean tester = true;
        try {
            department.remove(randomEmployee);
        } catch (RemoveException e) {
            tester = false;
        }
        assertTrue(tester == false);
    }
}