package mappe.del1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HospitalTest {
    /**
     * Made some simple test to check some of the code
     */

    @Test
    void getHospitalName() {
        Hospital hospital = new Hospital("Emergency");
        assertTrue(hospital.getHospitalName()=="Emergency");
    }
}