package mappe.del1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {
    /**
     * Made some simple test to check some of the code
     */

    @Test
    void getAndSetDiagnosis() {
        Patient patient = new Patient("Charles","Baker", "04045755831");
        assertTrue(patient.getDiagnosis()=="");
        patient.setDiagnosis("He is sick");
        assertTrue(patient.getDiagnosis()=="He is sick");
    }
}